Marlin Firmware for CL260
The most important parameters to take into account are:

// Mechanical endstop with COM to ground and NC to Signal uses "false" here (most common setup).
const bool Z_MIN_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.
const bool Z_MIN_PROBE_ENDSTOP_INVERTING = false; // set to true to invert the logic of the endstop.

define Z_MIN_PROBE_USES_Z_MIN_ENDSTOP_PIN

define AUTO_BED_LEVELING_FEATURE // Delete the comment to enable (remove // at the start of the line)
define Z_MIN_PROBE_REPEATABILITY_TEST // If not commented out, Z Probe Repeatability test will be included if Auto Bed Leveling is Enabled.

define AUTO_BED_LEVELING_GRID

define LEFT_PROBE_BED_POSITION 5
define RIGHT_PROBE_BED_POSITION 190
define FRONT_PROBE_BED_POSITION 20
define BACK_PROBE_BED_POSITION 215

Here I use a 3 x 3 grid
define AUTO_BED_LEVELING_GRID_POINTS 3

and finally:
define X_PROBE_OFFSET_FROM_EXTRUDER -23 // X offset: -left +right [of the nozzle]
define Y_PROBE_OFFSET_FROM_EXTRUDER 20 // Y offset: -front +behind [the nozzle]
define Z_PROBE_OFFSET_FROM_EXTRUDER -1.0 // Z offset: -below +above [the nozzle]

// Useful to retract a deployable Z probe.
define Z_PROBE_END_SCRIPT "M280 P0 S90"

Pay attention to the Z_PROBE_OFFSET_FROM_EXTRUDER because you should measure this value (-1.0 in my case) after having levelled the bed with the command G29.

Moreover consider that the Z min endstop will not function as usual, so don't home the Z axis (so comment any reference to the Z homing in your start print gcode).

Usually, after the calibration) I do like this:
G28 X0 Y0
G29
G1 Z0 for homing the Z axis.